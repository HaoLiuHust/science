---
title: Genetics
date: 2019-08-27
url: /11sci/5-genetics/
---

NZQA Link: [https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=90948](https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=90948)

## Slides

1. [Reproduction](slides/1-reproduction/) [(PDF)](pdfs/1-reproduction.pdf)
2. [Genes, Chromosomes & DNA](slides/2-genes-chromosomes-dna/) [(PDF)](pdfs/2-genes-chromosomes-dna.pdf)
3. [Structure of DNA](slides/3-structure-of-dna/) [(PDF)](pdfs/3-structure-of-dna.pdf)
4. [Variation](slides/4-variation/) [(PDF)](pdfs/4-variation.pdf)
5. [Genetic Crosses](slides/5-genetic-crosses/) [(PDF)](pdfs/5-genetic-crosses.pdf)

[Download All PDFs](5-genetics.zip)