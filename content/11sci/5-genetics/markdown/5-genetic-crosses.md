---
title: Genetic Crosses
subtitle: 11SCI - Genetics
author: Finn LeSueur
date: 2019
theme: finn
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
- \usepackage{textcomp}
---

## Learning Outcomes

- Understand and use the terms “genotype and phenotype”, “homozygous and heterozygous” and “dominant and recessive”.
- Know how to carry out genetic crosses using punnet squares and determine genotypic and phenotypic ratios.

---

<iframe width="1206" height="678" src="https://www.youtube.com/embed/Mehz7tCxjSE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Some Definitions

- __Genotype:__ The genetic make-up of a cell that determines one of its characteristics
- __Phenotype:__ The observable characteristics of an individual due to its genotype

---

### Phenotype

![](assets/5-phenotype-eyes.jpg){width=80%}

---

### Genotype

![](assets/5-genotype.png){width=70%}

---

![](assets/5-phenotype-genotype.png){width=80%}

---

### How do we describe the genotype?

- We use the words __dominant__ and __recessive__ to describe alleles.
- What is an allele?

---

#### Dominant Alleles

- A dominant allele will produce a certain phenotype, even in the presence of other alleles.
- For example, a brown eyes allele will create brown eyes even when paired with a blue eyes allele.
- If __B__ is brown and __b__ is blue: __BB__ and __Bb__ will both produce brown eyes.

---

#### Recessive Alleles

- A recessive allele will only produce a certain phenotype when in the presence of another of the same allele.
- For example, a blue eyes allele will only create blue eyes when paired with another blue eyes allele.
- If __B__ is brown and __b__ is blue: __BB__ and __Bb__ will both produce brown eyes, while only __bb__ will produce blue.

---

- Recall __homo__ means same and __hetero__ means different
- __BB__ is __homozygous dominant__
- __Bb__ is __heterozygous dominant__
- __bb__ is __homozygous recessive__

---

### Punnet Squares

- We can use punnet squares to determine the probability that two parents will produce offspring with a particular genotype, or with a particular phenotype.
- The father goes at the top of the square, and the mother on the left.
- We fill out the known genotypes of the parents and then complete the square.

---

If dad is __heterozygous dominant__ for brown eyes and mum is __homozygous recessive__ for blue eyes, complete this punnet square in your books.

![](assets/5-punnet-square.jpg){width=80%}

