---
title: Velocity
subtitle: 11SCIE - Mechanics
author: Finn LeSueur
date: 2019
theme: Madrid
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

# What is Speed?

You know that someone who finishes a race in the least time is the fastest. That they have the highest speed.

__Speed is the amount of distance covered in some amount of time__.

---

## What is Speed: Let's be Precise!

Cars often have their speed displayed in the number of kilometers that they can travel in an hour (_kilometers per hour_).

__But__, in Physics we prefer to give speed in the __number of meters__ travelled in __one second__.

We call this _meters per second_.

---

## Determining Speed

In order to determine speed we need to know two things, both of which you measured on Friday!

__Distance__: measured using a ruler, or tape measure, and measured in __meters__ (m).

__Time__: measured using a stopwatch and measured in __seconds__ (s).

---

## Symbols and Units

__Distance__: has unit meters (m) and is represented by symbol __d__ in equations.

__Time__: has unit seconds (s) and is represented by symbol __t__ in equations.

---

# Speed & Velocity

__Speed__ is a measure of distance covered in a certain amount of time.

e.g 13 meters per second

__Velocity__ includes a direction

e.g. 13 meters per second __east__

---

## How to Calculate Velocity

\begin{align*}
    & velocity = \frac{\text{change in distance}}{\text{change in time}} \\
    & v = \frac{\Delta d}{\Delta t} \\
\end{align*}

it is measured in meteres per second, also shown as $\frac{m}{s}$ or $ms^{-1}$.

---

# How Do We Solve Problems?

1. Knowns
2. Unknowns
3. Formula
4. Substitute
5. Solve

---

## Question 1: Velocity

A rock climber climbs 15 meters in 5.48 seconds. What velocity was he travelling?

---

## Question 1: Answer

A rock climber climbs 15 meters in 5.48 seconds. What velocity was he travelling?

\begin{align*}
    & v = \frac{\Delta d}{\Delta t} && \text{show the equation} \\
    & v = \frac{15}{5.48} && \text{substitute values} \\
    & v = 2.74\frac{m}{s}
\end{align*}

---

## Question 2: 

Mr LeSueur ran 4.61km in 21 minutes and 16 seconds. How fast did he run?

---

## Question 2: Answer

__Step 1__: Convert to SI units

- $d = 4.61 \times 1000 = 4610m$
- $t = (21 \times 60) + 16 = 1276s$
- $v = ?$

__Step 2__: 

\begin{align*}
    & v = \frac{\Delta d}{\Delta t} && \text{formula} \\
    & v = \frac{4610}{1276} && \text{substitute} \\
    & v = 3.61\frac{m}{s} && \text{solve}
\end{align*}

---

# Questions: sciPAD Page 8, 9, 10

---

# Distance-Time Graphs

We can plot a graph with __time on the x-axis__ and __distance on the y-axis__ to help us visualise data. Graphs are very useful to easily gain information from measurements.

![Distance-Time Graph](assets/1-distance-time-graph.png "Distance-Time Graph"){ width=60% }

---

## Interpreting Distance-Time Graphs

![Distance-Time Graph](assets/1-distance-time-graph.png "Distance-Time Graph"){ width=60% }

What can you tell me about the velocity of the runner in seconds A, B, C, D and E?

---

## Interpreting Distance-Time Graphs

![Distance-Time Graph](assets/1-distance-time-graph.png "Distance-Time Graph"){ width=60% }

- __Positive gradient__: Moving away
- __Flat gradient__: Stationary
- __Negative gradient__: Moving towards

---

## Drawing Graphs

![Distance-Time Graph](assets/1-distance-time-graph.png "Distance-Time Graph"){ width=30% }

- Start the axis at zero
- Title each axis
- Units on each axis
- Title the graph
- Use a ruler
- Make your scale even

---

# sciPAD Page 11, 12, 13

---

# Calculating Velocity From Graphs

\begin{align*}
    & v = \frac{\Delta d}{\Delta t} \\
    & v = \frac{rise}{run}
\end{align*}

![Distance-Time Graph](assets/1-distance-time-graph.png "Distance-Time Graph"){ width=60% }