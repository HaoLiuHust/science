---
title: Acceleration
subtitle: 11SCIE - Mechanics
author: Finn LeSueur
date: 2019
theme: Madrid
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

# What is Acceleration?

---

Acceleration is how quickly the velocity changes.

__e.g.__ A supercar will accelerate to 50km/hr faster than a cyclist. That is to say, the supercar has a greater acceleration.

---

<iframe width="1022" height="748" src="https://www.youtube.com/embed/1pfrYC0vagk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Calculating Acceleration

\begin{align*}
    & acceleration = \frac{\text{change in speed}}{change in time} \\
    & a = \frac{\Delta V}{\Delta t} \\
\end{align*}

- __Velocity__ has units meters per second ($ms^{-1}$)
- __Time__ has units seconds ($s$)
- __Acceleration__ has units meters per second per second ($m/s^{2}$ or $ms^{-2}$)

---

![Distance-Time Graph](assets/2-starter.png "Distance-Time Graph"){ width=90% }

---

## Rearranging Equations

\begin{align*}
    & a = \frac{\Delta V}{\Delta t} && \text{v is divided by t} \\
    & a \times \Delta t = \Delta V && \text{Undo the divide by multiplying}
\end{align*}


\begin{align*}
    & v = \frac{\Delta d}{\Delta t} && \text{d is divided by t} \\
    & v \times \Delta t = \Delta d && \text{Undo the divide by multiplying}
\end{align*}

---

### Using Formula Triangles: Velocity

If you are having a hard time with maths, you can write your formula into triangles like this. Multiplication on the bottom and the product on the top. You will __not__ be given formula in this form, you will have to remember the triangles if you want to use them.

![Velocity Triangle](assets/2-velocity-triangle.png "Velocity Triangle"){ width=60% }

---

### Using Formula Triangles: Acceleration

![Acceleration Triangle](assets/2-acceleration-triangle.jpg "Acceleration Triangle"){ width=60% }

---

# Velocity-Time Graphs

Much like we can plot a distance-time graph, we can also plot velocity-time graphs.

Velocity-time graphs are useful for determining whether an object is accelerating and for calculating the distance travelled.

![Velocity-Time Graph](assets/2-velocity-time-graph.png "Velocity-Time Graph"){ width=60% }

---

## Calculating Acceleration

![Velocity-Time Graph](assets/2-velocity-time-graph.png "Velocity-Time Graph"){ width=60% }

\begin{align*}
    & a = \frac{\Delta v}{\Delta t} \\
    & a = \frac{rise}{run} && \text{using a velocity-time graph}
\end{align*}