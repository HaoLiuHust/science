---
title: Pressure
subtitle: 11SCI - Mechanics
author: Finn LeSueur
date: 2019
theme: finn
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

# Starter

Calculate the __net__ force acting upon these objects:

![Unbalanced Forces](assets/4-unbalanced-forces.png)

---

## Question 1

What does an unbalanced force do to the motion of an object?

---

### Answer 1

It causes the object to accelerate (or deaccelerate).

We know this because of the equation

\begin{align*}
    & force = mass\times acceleration \\
    & F = m \times a
\end{align*}

---

## Question 2

For the equation

\begin{align*}
    & F = m \times a
\end{align*}

give the __name__ and __unit__ for each variable in the equation.

---

### Answer 2

- F stands for __force__ and has units __Newtons (N)__
- m stands for __mass__ and has units __kilograms (kg)__
- a stands for __acceleration__ and has units __meters per second squared ($ms^{-2}$)__

---

## Question 3

- What do you think of when you think of pressure?
- What affects the pressure exerted by an object?

---

### Question 4

The most classic case of pressure is sharpening a knife. Why do we sharpen knives? What does sharpening a knife change about the knife?

---

# Pressure

Pressure is defined as: _the amount of force per unit area_.

\begin{align*}
    & pressure = \frac{force}{area} \\
    & P = \frac{F}{A}
\end{align*}

- Force is measured in __Newtons (N)__
- Area is measured in __meters squared ($m^{2}$)__

---

# Pressure Continued

\begin{align*}
    & P = \frac{F}{A}
\end{align*}

- Therefore, pressure is measured in Newtons / meters squared ($\frac{N}{m^{2}}$ OR $Nm^{-2}$)
- This is also known as a Pascal (Pa)

---

## Starter (2018 Exam)

Jacob bikes down The Flying Nun. He and his bike have a mass of $82kg$ and he accelerates at $0.8ms^{-2}$.

1. Calculate the __net force__ acting upon Jacob and his bike to cause this acceleration
2. Draw a force diagram showing the forces acting upon Jacob as he accelerates
3. Describe the size and direction of the forces compared to each other (horizontal and vertical)

---

## Calculating Pressure

\begin{align*}
    & P = \frac{F}{A}
\end{align*}

1. Calculate the __pressure__ created by a force of $3N$ with an area of $0.5m^{2}$
2. Calculate the __pressure__ created by a force of $3N$ with an area of $0.25m^{2}$
3. Calculate the __pressure__ created by a force of $3N$ with an area of $0.125m^{2}$

---

### Answers

\begin{align*}
    & P = \frac{F}{A} \\
    & \text{1. } P = \frac{3}{0.5} = 6Pa \\
    & \text{2. } P = \frac{3}{0.25} = 12Pa \\
    & \text{3. } P = \frac{3}{0.125} = 24Pa
\end{align*}

---

## Calculating Area

\begin{align*}
    & P = \frac{F}{A}
\end{align*}

4. Calculate the __area__ that a force ($5N$) is acting through if it has a pressure of $7Pa$
5. Calculate the __area__ that a force ($7N$) is acting through if it has a pressure of $3Pa$
6. Calculate the __area__ that a force ($10N$) is acting through if it has a pressure of $3Nm^{-2}$

---

### Answers

\begin{align*}
    & A = \frac{F}{P} \\
    & \text{4. } A = \frac{5}{7} = 0.71m^{2} \\
    & \text{5. } A = \frac{7}{3} =  2.33m^{2} \\
    & \text{6. } A = \frac{10}{3} = 3.33m^{2}
\end{align*}

---

## Calculating Force

\begin{align*}
    & P = \frac{F}{A}
\end{align*}

7. Calculate the __force__ of an object with area $0.25m^{2}$ and pressure $5Pa$
8. Calculate the __force__ of an object with area $0.5m^{2}$ and pressure $7Pa$
9. Calculate the __force__ of an object with area $0.75m^{2}$ and pressure $10Pa$

---

### Answers

\begin{align*}
    & F = P \times A \\
    & \text{7. } F = 0.25 \times 5 = 1.25N \\
    & \text{8. } F = 0.5 \times 7 = 3.5N \\
    & \text{9. } F = 0.75 \times 10 = 7.5N
\end{align*}

---

## Exam Question (2018)

Giovanni is running a 100m sprint. Each of his feet have a surface area of $200cm^{2}$ ($0.0200m^{2}$), which sink into the track. Together, his feet exert a pressure of $13000Pa$. Calculate Giovanni's weight.

---

### Answer

Because __weight__ is a force, we know that we are looking for $F$.

\begin{align*}
    & P = 13000Pa
    & A = 0.02m^{2} \times 2 = 0.04m^{2} \\
    & F = P \times A \\
    & F = 13000 \times 0.04 \\
    & F = 520N
\end{align*}

---

## Extra Work

- __Homework:__ Education Perfect due Monday 29th 11:25am
- sciPAD Page 46
