---
title: Force
subtitle: 11SCI - Mechanics
author: Finn LeSueur
date: 2019
theme: finn
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

# Starter

Brainstorm situations where force is involved, things that cause force and how it can be used in a __Physics__ context on the board!

---

# Force

Force has lots of applications in our world! Everything from cars, to aeroplanes, tug-of-war, sports and even bio-mechanics!

---

<iframe width="1206" height="678" src="https://www.youtube.com/embed/OmOEnfQk3bw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Defining Force

Force is a __push__ or a __pull__ and is measured in __Newtons (N)__.

Forces have a size (1, 2, 3, 4) and a direction (left, right, up, down).

---

## How Forces Act

Consider you sitting on your seat. What forces are acting upon you?

Draw a box to represent yourself, with arrows coming out of the box to represent the forces. Make sure to label them!

---

## Force Diagram

![Force Diagram](assets/3-force-diagram.png)

---

### Force Diagram

- The length of the arrow represent the __size__ of the force
- The direction of the arrow represents the direction of the force
- Arrows should all be labelled with names and sizes if possible

---

## Balanced Forces

Think and discuss with the people around you:

Sitting on your chair, are the forces acting on you balanced or unbalanced? How do you know? What does it feel like?

---

### Vertical and Horizontal Forces

- Vertical and horizontal forces are separate. They do not affect each other.
- We can _balance_ them to find out the __net force__ in the vertical and horizontal directions.
- If we compare them and they are the same, then forces are balanced. If they are different, forces are unbalanced.

---

<iframe width="1206" height="678" src="https://www.youtube.com/embed/GJ4Qp2xeRds" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

# sciPAD Page 33-35