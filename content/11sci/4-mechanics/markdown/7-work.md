---
title: Power & Work
subtitle: 11SCI - Mechanics
author: Finn LeSueur
date: 2019
theme: finn
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

# 2018 Exam

Giovanni has a mass of $75kg$ and is doing a ski jump. At the top of his flight he has $3200J$ of gravitational potential energy.

1. Calculate his downward (vertical) speed just before he lands (assuming energy is conserved).
2. Explain why Giovanni's actual speed is lower than calculated in 1.

---

# Work

When you lift your backpack off the ground you are transferring some __chemical potential energy__ in your muscles into __gravitational potential energy__ in the backpack.

This transfer of energy is called __work__.

---

Other examples including the launching of a rocket, the falling of a skydiver, the riding of a rollercoaster.

---

## Example 1

Sam has decided to take up weight lifting and starts by deadlifting a $20kg$ mass from the ground to a height of $1m$. Calculate the work done for Sam to lift the weight.

Hint: Calculate the gravitational potential energy at the start and end of the lift.

---

## Example 2

Jack has gone hiking up Avalanche Peak. He has a mass of $55kg$ and reaches a height of $1833m$, starting from $733m$. How much work has Jack done to reach the top?

---

## 2018 Exam

Sophie is snow skiing and uses a ski tow to get to the top of the slope.
The ski tow pulls Marama up the slope to a height of 46.2 m.
The combined mass of Sophie and her ski gear is 62 kg.

Calculate the work done for Marama to reach the top of the slope.

---

# Calculating Work

__Recall__: Work is the amount of energy transferred

In the vertical direction, work can be calculated using this formula

\begin{align*}
    & work = force \times distance \\
    & W = F \times d
\end{align*}

---

If you are uncomfortable using this formula:

\begin{align*}
    & W = F \times d
\end{align*}

just use the potential energy calculation because they are just different expressions of each other.

\begin{align*}
    & E_{p} = m \times g \times \Delta h
\end{align*}

---

# Starter

A rocket is launched with an acceleration of $90ms^{-2}$. It has a mass of $5kg$ and it accelerates until it reaches a height of 2000m. How much work is the rocket thruster doing?

---

# Power

- Power is the rate at which energy is transferred.
- This is different to work which is the __amount__ of energy transferred.
- A higher power means more energy is transferred each second.
- A lower power means less energy is transferred each second.

---

- Power is measured in Joules per second ($J/s$).
- Since Joules is the unit for energy, we know this means energy per second.

\begin{align*}
    & power = \frac{work}{time} \\
    & P = \frac{W}{t} \\
\end{align*}

---
