---
title: Kinetic Energy
subtitle: 11SCI - Mechanics
author: Finn LeSueur
date: 2019
theme: finn
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

# Starter (2016 Exam)

Rhiana is riding a horse along New Brighton Beach. __Each__ of the horse's hooves have a surface area of $0.0044m^{2}$ which sink into the sand when the horse stops. The horse exerts a pressure of $200155Pa$.

__Calculate the weight of the horse.__


---

## Answer

A horse has four hooves, so the total surface area that the horse is exerting pressure through is:

\begin{align*}
    & A = 0.0044 \times 4 = 0.0176m^{2}
\end{align*}

Recall that  __weight is a force__, so we  can rearrange $P=\frac{F}{A}$ for $F$.

\begin{align*}
    & F = P \times A \\
    & F = 200155 \times 0.0176 = 3522.728N
\end{align*}

---

# Kinetic Energy

<iframe width="1206" height="678" src="https://www.youtube.com/embed/PWNs7i4rEWA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## What is Kinetic Energy?

Kinetic energy is the energy that an object posesses due to its __velocity__!

---

## What Other Types of Energy Are There?

There are many types of energy in the universe:

- Gravitational potential energy
- Heat energy
- Sound energy
- Light energy
- Elastic potential energy
- Electrical energy

---

### What do they have in common?

- All energy is measured in __Joules__ ($J$).
- A change in energy is called __work__  (measured in Joules).

---

## Conservation of Energy

- Energy cannot be created or destroyed, only transformed.
- This means that gravitational potential energy can be transformed into kinetic energy, and kinetic energy into other forms of energy. It is always _taken_ from somewhere, never created from nothing.

---

## Calculating Kinetic Energy

Kinetic energy depends on the __mass__ and __velocity__ of an object.

\begin{align*}
    & E_{k} = \frac{1}{2} \times m \times v^{2}
\end{align*}

---

### What does $v^{2}$ mean?

- It means $v \times v$
- This means we can also write the equation like this, if you find it easier:

\begin{align*}
    & E_{k} = \frac{1}{2} \times m \times v \times v
\end{align*}

---

### Example 1

Mr LeSueur rides his bike to work at $32km/h$ ($8.89ms^{-1}$). Both he and his bike have a combined mass of $80kg$. __Calculate his kinetic energy__.

---

#### Answer

We know $m=80kg$ and $v=8.89ms^{-1}$, and we are looking for $E_{k}$.

\begin{align*}
    E_{k} = \frac{1}{2} \times 80 \times 8.89^{2} \\
    E_{k} = 3161.284J
\end{align*}

---

### Example 2

Sophie is skiing down Upper Fascination at Mt Hutt, and is trying to go really fast. Her combined mass is $60kg$ and she is moving at $60km/h$ ($16.67ms^{-1}$). __Calculate her kinetic energy__.

---

#### Answer

We know $m=60kg$ and $v=16.67ms^{-1}$, and we are looking for $E_{k}$.

\begin{align*}
    E_{k} = \frac{1}{2} \times 60 \times 16.67^{2} \\
    E_{k} = 8336.667J
\end{align*}

---

<iframe width="560" height="321" src="https://www.youtube.com/embed/dYw4meRWGd4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Extra Work

- __Homework:__ Education Perfect due Monday 29th 11:25am
- sciPAD Page 46
