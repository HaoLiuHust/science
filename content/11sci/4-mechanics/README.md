# Year 11 Science 1.1: Mechanics (90940)

NZQA Link: [https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=90940](https://www.nzqa.govt.nz/ncea/assessment/view-detailed.do?standardNumber=90940)

## Learning Outcomes

__Motion__

1. Name the metric units of __distance__, __time__ and __speed__ and give their symbols.
2. Define the term speed.
3. Use simple instruments such as rulers and stopwatches to measure distance and time.
4. Calculate the __average speed__ of objects. Use $`v=d/t`$.
5. Draw __distance/time graphs__ from data obtained in experiments.
6. Use the slope of a distance/time graph to describe and/or calculate the speed of an object.
7. Draw __speed/time__ graphs from given data or data obtained in motion experiments.
8. Use the slop of a speed/time graph to describe and/or calculate the speed of an object.
9. Define __acceleration__ including its unit and symbol.
10. Calculate acceleration from speed and time data and speed/time graphs. Use $`a = v/t`$.
11. Calculate the distance covered by an object using a speed/time graph of its motion.

__Force__

12. Identify examples of __forces__ and represent them in __force diagrams__.
13. Describe the different effects of __balanced__ and __unbalanced__ forces on the motion of an object and recognise examples of __equilibrium__.
14. Name the units of measurement for __force__, __mass__ and __acceleration__ and give their symbols.
15. Describe the relationship between mass and acceleration of an object which is acted iupon by a given unbalanced force (called __net force__).
16. Use the relationship $`F = ma`$ to calculate the net force, mass or acceleration of an object.
17. Define the __weight__ of an object.
18. Use a balanced to compare mass and weight.
19. Define the force of __friction__.
20. Describe the energy effects of friction on stationary and/or moving objects.
21. Explain force and pressure in terms of everyday situations. Use $`P = F/A`$.
22. Describe the diffferent forms of energy including heat, kinetic, gravitrational, potential, elastic poten tial, sound and solar energies.
23. Identify energy changes.
24. Give the symbols and units for kinetic and potential energy.
25. Use $`E_{k} = \frac{1}{2}mv^{2}`$
26. Explain the conservation of mechanical energy in free fall situations (e.g. ball sports)

__Work and Power__

27. Define __work__.
28. Name the unit of work and give its symbol.
29. Use $`W = Fd`$.
30. Use $`Ep = mgh`$.
31. Use $`W = Fd`$ and $`Ep = mgh`$ to determine amounts of energy transfer.
32. Define __power__.
33. Name the unit of power and give its symbol.
34. Calculate the power of a device from given data.