---
title: Electricity
date: 2019-08-27
url: /12phy/3-electricity/
---

## Slides

1. [Static Electricity](slides/1-static-electricity.html) [(PDF)](pdfs/1-static-electricity.pdf)
2. [Electric Fields](slides/2-electric-fields.html) [(PDF)](pdfs/2-electric-fields.pdf)
3. [Charge and Current](slides/3-charge-and-current.html) [(PDF)](pdfs/3-charge-and-current.pdf)
4. [Voltage and Power](slides/4-voltage-and-power.html) [(PDF)](pdfs/4-voltage-and-power.pdf)
5. [Resistance and Ohm's Law](slides/5-resistance-and-ohms-law.html) [(PDF)](pdfs/5-resistance-and-ohms-law.pdf)
6. [Dangers of Electricity](slides/6-dangers-of-electricity.html) [(PDF)](pdfs/6-dangers-of-electricity.pdf)
7. [Series and Parallel Circuits](slides/7-series-and-parallel-circuits.html) [(PDF)](pdfs/7-series-and-parallel-circuits.pdf)
8. [Term 2 Revision](slides/99-term-2-revision.html) [(PDF)](pdfs/99-term-2-revision.pdf)
9. [Magnetic Fields](slides/8-magnetic-fields.html) [(PDF)](pdfs/8-magnetic-fields.pdf)
10. [Magnetism](slides/9-magnetism.html) [(PDF)](pdfs/9-magnetism.pdf)
11. [Motors & Inductors](slides/10-motors-and-inductors.html) [(PDF)](pdfs/10-motors-and-inductors.pdf)

[Download All PDFs](3-electricity.zip)
