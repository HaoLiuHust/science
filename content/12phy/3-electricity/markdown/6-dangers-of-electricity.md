---
title: The Dangers of Electricity
subtitle: 12PHYS - Electricity
author: Finn LeSueur
date: 2019
theme: finn
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

# Starter

Play with the simulation linked in Google Classroom and __brainstorm situations where static electricity is dangerous and how to minimise the danger.__

[PhET: Balloons & Static Electricity](https://phet.colorado.edu/en/simulation/balloons-and-static-electricity)

---

## Do You Remember:

1. How do you make something positively charged or negatively charged?
2. Explain why the hair sticks to the ballooon
3. Explain why the hair stands up when you are touching a Van der Graaf Generator
4. Draw a charge diagram showing how a piece of paper is attracted to a positively charged rod

---

![Static Electricity Joke](assets/6-static-shock.png)

---

### Protecting Buildings

![Lightning Rod](assets/6-lightning-rod.jpg)

---

### Protecting Electrical Devices

![Static Devices](assets/6-static-devices.jpg)

# What Have We Learned?

- How to describe when static electricity can be dangerous