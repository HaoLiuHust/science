---
title: Motors & Inductors
subtitle: 12PHYS - Electricity
author: Finn LeSueur
date: 2019
theme: finn
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

## Starter

1. What is a magnetic field?
2. What is the symbol and unit for magnetic field?
3. What formula is used to calculate the size of the force?
4. How can you determine the direction of the force?

---

\begin{align*}
    & F = BIL
\end{align*}

Force on a current carrying wire in a magnetic field is given by this equation.

- Write what each letter stands for and its unit

---

1. What direction is the force on this current carrying wire?
2. What is the force if the field is $0.02T$, the current $2A$ and the length of wire $10cm$?

![](assets/10-current-wire.png){width=60%}

---

What do you think happens to the force if the wire is on an angle?

![](assets/10-angle-current-wire.png){width=80%}

---

\begin{align*}
    & F = BILsin(\theta)
\end{align*}

- $\theta$ is the angle between the current and the magnetic field
- Force is maximum when perpendicular
- Force is zero when parallel

---

Find the force on this wire in a field of $0.05T$ with a current of $0.15mA$ flowing through it. The length of the wire is $20cm$ and the angle is $38^{\circ}$.

![](assets/10-angle-wire-example.png){width=80%}

---

## DC Motors

- What does a motor do?
- It takes power and turns it into rotational energy! How?

---

What direction is the force acting upon the four sides of this loop? What does this tell you?

![](assets/10-dc-motor-rotate.png){width=80%}

---

![](assets/10-dc-motor-explain.png){width=80%}

---

## Starter 1

![](assets/10-starter-1.png){width=70%}

---

## Starter 2

![](assets/10-starter-2.png){width=70%}

---

## Starter 3

![](assets/10-starter-3.png){width=70%}

---

## Starter 4

![](assets/10-starter-4.png){width=70%}

---

### What happens if we pass a wire through a magnetic field?

---

We induce a voltage!

![](assets/10-electromagnetic-induction.gif){width=90%}

---

__Induced voltage__ in a wire moving through a magnetic field:

\begin{align*}
    & V=BvL
\end{align*}

- $B$ is the magnetic field strength (T, Tesla)
- $v$ is the velocity of the wire ($ms^{-1}$)
- $L$ is the length of wire in the field ($m$)

---

### Example

A metal rod is moved in a magnetic field. The rod is $24cm$ long and moves at $8ms^{-1}$ through a magnetic field with strength $0.7T$. __Calculate the induced voltage and induced current__.

![](assets/10-vbil-example.png)

---

### Thought Experiment

- Dragging a piece of wire through a magnetic field creates an __induced voltage__
- If the circuit is complete it creates an __induced current__
- Recall how a current carrying wire acts in a magnetic field?
- Yes! $F=BIL$

![](assets/10-induction-example.png){width=50%}

---

If we drag the coil to the right, what will we observe?

![](assets/10-induction-example.png){width=80%}

---

## Lenz's Law

- __The induced current always opposes the change/action producing it__
- In our previous example the induced current causes a force which opposes the movement of the wire
- This law is a result of __the conservation of energy__. Work must be done to generate electrical energy.

---

Continuing our example from earlier: a metal rod is moved in a magnetic field. The rod is $24cm$ long and moves at $8ms^{-1}$ through a magnetic field with strength $0.7T$. __Calculate the magnitude and direction of the opposing force acting upon the rod__.

![](assets/10-vbil-example.png)
