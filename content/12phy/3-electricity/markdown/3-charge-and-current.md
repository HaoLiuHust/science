---
title: Charge & Current
subtitle: 12PHYS - Electricity
author: Finn LeSueur
date: 2019
theme: finn
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

# Starter

1. Define electric field
2. Draw a uniform electric field between two plates. Make the top plate __negatively charged__
3. Describe the motion of a negatively charged object in the electric field
4. If the negative charge moves towards the negative plate, does it gain or lose electric potential energy?

---

# Question: What are some common carriers of charge?

---

## Answer

__Electrons__ are the charge carriers in metals, ions in solution, electrically charged gas in plasma.

---

# Conductors

A conductor is a material through which charge can move freely.

__e.g. electrons move through metal__

---

## Current

Current is the flow of charge (often electrons).

It is the measure of the rate at which the charge flows (Amperes).

__Recall:__ $1C$ of charge is $6.25\times10^{18}$ electrons

---

\begin{align*}
    & I = \frac{q}{t} \\
    & \text{I = current measured in what?} \\
    & \text{q = charge measure in what?} \\
    & \text{t = time measured in what?}
\end{align*}

---

### Examples

1. If $10A$ flows through a wire, how much charge passes a point in $5s$?
2. A total charge of $0.12C$ passes a point in $5s$. What is the current?
3. $20C$ of charge passed through a light bulb in $4s$. What was the current?
4. $0.02C$ of charge passed through a resistor in 1 minute. What was the current?
5. If the current is $0.3A$, how much charge will pass a point in 10 minutes?

---

### What Direction Does Current Flow?

![Circuit Diagram](assets/3-current.jpg "Circuit Diagram"){ width=50% }

---

### Conventional Current

Conventional current is the direction that positive charges move in a circuit. From the positive terminal to the negative terminal.

However, electrons actually move from the negative terminal to the positive terminal, which is __opposite__ to _conventional current_.


