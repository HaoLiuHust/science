---
title: Magnetic Fields
subtitle: 12PHYS - Electricity
author: Finn LeSueur
date: 2019
theme: finn
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

# Starter

The battery is 12V.

![](assets/8-circuit.jpg){width=30%}

1. How many joules of energy does the cell supply to each coulomb of charge that flows out of the cell? __(A)__

---

![](assets/8-circuit.jpg){width=30%}

2. When the cell is switched on, the resistance of the lamp is $9\Omega$. Calculate the current flowing through the lame. __(A)__
3. State the meaning of the term __resistance__ in terms of electron flow. __(A)__

---

# Magnetic Fields

Magnetic fields can be found in two places:

- Around magnetic objects
- Around current carrying wires

---

![](assets/8-bar-magnet.jpg)

---

![](assets/8-iron-filings.jpg)

---

![](assets/8-attraction-repulsion.png)

---

<iframe width="1206" height="678" src="https://www.youtube.com/embed/aVqN1tW1k7w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

![](assets/8-earth-magnetic-field.jpg){width=75%}

---

![](assets/8-field-wire.jpg){width=60%}

---

## Right Hand Grip Rule 1

![](assets/8-right-hand-rule-1.gif){width=75%}

---

![](assets/8-field-in-and-out.png)

---

# Solenoids

![](assets/8-solenoid.jpg){width=60%}

\begin{align*}
    & B = \mu_{0} n I \\
\end{align*}

---

## What is $\mu_{0}$?

It is pronounced _mew nought_ and goes by a variety of names: __vacuum permeability__, __permeability of free space__, __permability of vacuum__,  __magnetic constant__.

I will endevour to call it is the __permeability of free space__ and it has the value:

\begin{align*}
    & \mu_{0} = 1.256 637 062 12(19)\times10^{-6} H/m
\end{align*}

---

## Right Hand Grip Rule 2

![](assets/8-right-hand-grip-2.png){width=50%}

---

![](assets/8-magnetic-p-table.jpg){width=75%}

---



