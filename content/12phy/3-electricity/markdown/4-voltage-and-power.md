---
title: Voltage & Power
subtitle: 12PHYS - Electricity
author: Finn LeSueur
date: 2019
theme: finn
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

# Starter

1. Draw a series circuit with 6V power supply, a bulb and a resistor. Indicate the positive and negative terminals on the power supply, and indicate the direction of conventional current and the actual movement of electrons.
2. What is the definition of current?
3. Give an equation that relates to the definition.
4. A circuit draws 0.4A and in total 2.5C goes past a certain point. How long did was the circuit on for?

---

Yesterday we talked about current and how it is the rate of transfer of charge per unit time.

\begin{align*}
    & I = \frac{q}{t}
\end{align*}

---

# What is a circuit?

![Circuit Diagram](assets/4-basic-circuit.png "Circuit Diagram"){ width=50% }

---

## Answer

A circuit is a way to deliver energy to different components!

[PhET DC Circuit Construction Simulation](https://phet.colorado.edu/sims/html/circuit-construction-kit-dc/latest/circuit-construction-kit-dc_en.html)

---

# Voltage

- The __charge carriers__ flowing around a circuit are __energy carriers__
- The __charge carriers__ carry __electrical energy__ which comes from the power supply
- __Voltage is the amount of energy in one coulomb of charge__

\begin{align*}
    & V = \frac{E_{p}}{q} \\
    & V = \text{ voltage measured in...} \\
    & E_{p} = \text{ electrical energy measured in... } \\
    & q = \text{ charge measured in...}
\end{align*}

---

# Power

- The amount of energy transformed per second
- E.g. A 100W light bulb transforms 100J of electrical energy per second into light and heat energy.

\begin{align*}
    & P = IV \\
    & P = \text{ power measured in...} \\
    & I = \text{ current measured in...} \\
    & V = \text{ voltage measured in...}
\end{align*}

---

# Voltage & Power Summary

\begin{align*}
    & I = \frac{q}{t} \\
    & V = \frac{E_{p}}{q} \\
    & \\
    & P = I \times V = \frac{q}{t} \times \frac{E_{p}}{q} = \frac{E_{p}}{t}
\end{align*}