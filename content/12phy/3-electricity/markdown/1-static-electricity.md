---
title: Static Electricity & Charge
subtitle: 12PHYS - Electricity
author: Finn LeSueur
date: 2019
theme: finn
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

---

# Starter

Brainstorm with the people around you what you know about electricity from Year 9 and 10.

Write it on the board when ready!

---

# Static Electricity

Static electricity has many hilarious effects, from Tesla coils, to lightning & making your hair stand on end. But it all depends on charge!

![Static Electricity](assets/1-static-electricity.jpg "Static Electricity"){ width=50% }

---

## Charges: Positive & Negative

We should all remember that, much like magnets, opposites attract and likes repel.

![Electric Charges](assets/1-charges.png "Electric Charges"){ width=75% }

---

## Charge Carriers

Recall: __the atom__. What element is this? What are the three subatomic particles that make it up?

![](assets/1-carbon-atom.gif "Carbon atom"){ width=50% }

---

- __Electrons__: Negatively charged
- __Protons__: Positively charged
- __Neutrons__: No charge

__Question__: What happens when an atom loses or gains electrons?

---

## Ions

Electrons are extremely light and move very fast. Therefore they can sometimes escape an atom.

- An ion is formed when an atom gains or loses electrons.
- Losing one or more electrons makes you __positively charged__ and is called a __cation__.
- Gaining one or more electrons makes you __negatively charged__ and is called an __anion__.

---

### Question 1

What did we do in Year 10 Science to remove charges from one object and put them onto another?

---

### Question 1: Answer

We applied friction!

---

# Van der Graaf Generator

![Van der Graaf Generator](assets/1-van-der-graaf-generator.png "Van der Graaf Generator"){ width=50% }

---

# What is Charge?

In Physics the symbol for charge in equations is $q$ or $Q$.

__Unit__: Coulomb (C)

We use it to describe how positive or negative an object is.

---

## What's a Coulomb?

We know that electrons are negatively charged. In fact, they have a charge of $-0.00000000000000000016 Coulomb$ also written as $-1.6 \times  10^{-19}C$.

Therefore we can calculate that if an object has a charge of +1C, is has lost $6,250,000,000,000,000,000$ electrons.

$6.25 \times 10^{18} electrons = -1 C$

---

## Question 2 & 3

1. If a balloon has charge of $-3C$: did the balloon lose or gain electrons, and how many?
2. If Charlotte has charge of $0.2C$ did she lose or gain electrons and how many?

---

## Question 2 & 3 Answers

1. If a balloon has charge of $-3C$: did the balloon lose or gain electrons, and how many?

\begin{align*}
    & \text{Negative C means electrons are gained (negative charge)} \\
    & num_{e} = 3 \times (6.25 \times 10^{18}) \\
    & num_{e} = 1.875 \times 10^{19} && \text{ electrons gained}
\end{align*}


2. If Charlotte has charge of $0.2C$ did she lose or gain electrons and how many?

\begin{align*}
    & \text{Positive C means electrons are lost (positive charge)} \\
    & num_{e} = 0.2 \times (6.25 \times 10^{18}) \\
    & num_{e} = 1.25 \times 10^{18} && \text{ electrons lost}
\end{align*}
