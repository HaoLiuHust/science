---
title: Projectile Motion
subtitle: 12PHYS - Mechanics
author: Finn LeSueur
date: 2019
theme: finn
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
---

# Do Now: Vector Recap
1. Yssy travels 30km south and then 20km west. Draw a vector diagram to show her total displacement.
2. Max and Lena are pushing a box. Max is pushing it with force 500N to the right, and Lena is pushing it with force 400N up. Draw a vector diagram to show the net force.
3. Phoebe is flying at $7ms^{-1}$ east. Phoebe changes direction so she flying at $7ms^{-1}$ south. Draw a vectoer diagram to show the change in velocity of Phoebe.

---

# Projectile Motion
Projectile motion can be thought of as _motion under gravity_.

![](assets/ideal-world-meme.jpg){ width=75% }

---

# Step 1: Describing Velocity
To get into projectile motion we first need to correctly describe the velocity and acceleration of an object in motion.

A ball is thrown vertically upwards. __Describe the direction of the ball's velocity when:__

1. It is going up,
2. it is going down,
3. it is at the highest point.

__Describe the direction of the ball's acceleration when it is:__

1. Going up,
2. going down,
3. at the highest point.

---

# Forces on The Ball
We assume that friction force is negligible (we ignore it).

Therefore, _the only force_ acting upon the ball while in the air is the *__weight force__*.

\begin{equation}
	F_{net} = F_{weight}
\end{equation}

The ball experiences a constant downwards acceleration ($-9.8ms^{-2}$) at all times.

---

# Acceleration Due to Gravity
\begin{equation}
	g = 9.8ms^{-2}
\end{equation}

The acceleration of any object in the air without its own power source.

---

# So, Projectile Motion

- An object what moves through the air __without its own power source__,
- only force acting upon it is the __weight force__,
- always experiencing __downward acceleration of $9.8ms^{-2}$__.

---

# Question
A ball is thrown upwards with an inital speed of __161.3km/hr ($44.8ms^{-1}$)__.

1. How long does it take for the ball to reach its highest point?
2. How high does the ball rise?

__Remember:__ Knowns, Unknowns, Formula, Substitute, Solve

---

# Question
Lachie kicks a rugby ball straight upwards. It is in the air for __10.6s__ before it hits the ground.

1. What is the inital velocity of the ball?
2. How high does the ball rise?

__Remember:__ Knowns, Unknowns, Formula, Substitute, Solve

---

# Question
Angus is going cliff diving. He jumps and falls for __3.4s__ before hitting the water below.

1. What is his __initial velocity__?
2. What is his __acceleration__?
3. What is his __final velocity__ (as he hits the water)?
4. How __high__ is the cliff?

__Remember:__ Knowns, Unknowns, Formula, Substitute, Solve

---

# A Classic: The Cannon Ball Question

A cannon ball is fired horizontally from the top of a hill.
The velocity of the cannon ball is split into two components:

1. __Vertical motion__, and
2. __horizontal motion__.

__Note:__ These motions are __independent of each other__. That is, they do not affect each other.

---

# Vertical Motion

__Recall:__ once in motion, the only force acting upon the cannon ball is the __weight force__. We are assuming friction is negligible.

Therefore:

The cannon ball is experiencing a constant downward acceleration of $g=-9.8ms^{-2}$.

---

# Horizontal Motion

__Recall:__ once in motion, the only force acting upon the cannon ball is the __weight force__. We are assuming friction is negligible.

Therefore, the cannon ball does not experience any forces in the horizontal direction. Therefore it does not accelerate in the horizontal direction.

---

# Summary

\begin{equation}
	Projectile Motion = Vertial Motion + Horizontal Motion
\end{equation}

## Vertical Motion
Constant downwards acceleration of $-9.8ms^{-2}$.

## Horizontal Motion
No acceleration, therefore constant speed. $v_{i} = v_{f}$

---

# Thought Experiment
If the cannon ball is fired with a __greated horizontal velocity__ will it take longer to hit the ground?

---

# Question
A marble rolls of a desk of height 1.25m and with $v_{x} = 1.6ms^{-1}$.

1. Calculate the __duration of the fall__
2. During the fall, how far does the marble travel __horizontally__?
3. How far does the marble travel if $v_{x} = 3ms^{-1}$?