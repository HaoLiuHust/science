---
title: Atoms and Isotopes
subtitle: 12PHY - Atomic and Nuclear Physics
author: Finn LeSueur
date: 2019
theme: finn
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
- \usepackage{textcomp}
---

# The Assessment

- Internal
- 3 credits
- Assessed on Week 1, Term 4
- Those away are assessed in Week 2 (or 3), Term 4

---

## Question: What is everything made up of?

- Elements!

---

<iframe width="1206" height="678" src="https://www.youtube.com/embed/d1EnW4kn1kg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

![](assets/1-four-nations.jpg)

- Aristotle and the Greeks in 450BC thought that everything was made of water, earth, air and fire

---

In the modern day, we now know that everything is made up of about 119 elements.

![](assets/1-periodic-table.png){width=75%}

---

## Question: What makes the elements different from each other?

- The number of protons contained inside them

---

We know that each element is made up of a unique type atom

![](assets/1-elements.png)

---

# Atoms

- Atoms are made of three subatomic particles.
- __Write their names, charges and a diagram of their location in your books__

---

## Rutherford's Atomic Structure

- The central region is called the nucleus. It is made up of protons and neutrons which are strongly bound together by nuclear force.
- Protons and neutrons are called nucleons.

---

- Electrons are constantly moving around the nucleus in an unknown way.
- Protons have positive charge, electrons have negative charge and neutrons are neutral.
- The nucleus is very small compare to the size of the whole atom, but has 99.95% of the mass (because electrons are very light).

---

Give the number of protons, neutrons, electrons and nucleons for __oxygen__, __sodium__, __aluminium__ and __copper__.

---

## Atomic and Mass Numbers

- Atomic number = the number of protons
- Mass number = the number of nucleons (protons AND neutrons)
- The number of protons in an atom (i.e. the atomic number) defines which type of atom it is. (e.g. an atom that has 6 protons MUST be a carbon atom, and cannot be any other)

---

Now add the atomic and mass numbers to __oxygen__, __sodium__, __aluminium__ and __copper__.

---

## Atoms vs Ions

- Every atom is neutral (has no overall charge). This is because they have the same number of protons and electrons. The positive charge of protons and the negative charge of electrons cancel each other out.

---

- However, an atom may lose (or gain) one or more electrons, in which case they become ‘ions’ – having a positive (or negative) charge. Losing or gaining electron(s) is called ionisation.
- Atoms are neutral, ions are charged.

---

![](assets/1-table.png)

---

# Isotopes

- Isotopes are the atoms that have the same number of protons (i.e. same element) but different numbers of neutrons.
- They have the same atomic number but different mass numbers.
- e.g. Carbon-12, Carbon-13, Carbon-14
- (the numbers 12, 13, 14 are mass numbers)

---

![](assets/1-carbon-isotopes.png)
