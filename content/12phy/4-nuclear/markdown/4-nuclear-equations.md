---
title: Nuclear Equations
subtitle: 12PHY - Atomic and Nuclear Physics
author: Finn LeSueur
date: 2019
theme: finn
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
- \usepackage{textcomp}
---

## Learning Outcomes

Be able to write equations for these nuclear reactions:

- Alpha decay/radiation/emission
- Beta decay/radiation/emission
- Gamma decay/radiation/emission

---

## Starter

In alpha emission, the decaying atom loses 2 protons and 2 neutrons.

This means that the atomic number of the decaying atom is ________ by ________ and the mass number is ________ by ________.

---

![](assets/4-a-decay-table-1.png){width=80%}

<aside class="notes">
    Fill out this table for alpha decay.
</aside>

---

![](assets/4-a-decay-table-2.png){width=80%}

---

\begin{align*}
    & {}^{214}_{81}Tl \rightarrow {}^{210}_{79}Au + {}^{4}_{2}He \\
    & {}^{214}_{81}Tl \rightarrow {}^{210}_{79}Au + {}^{4}_{2}\alpha \\
\end{align*}

---

In a beta emission, a neutron turns into a proton and emits a high-speed electron.

This means that the atomic number of the decay atom is ________ by ________ and the mass number _____________.

---

![](assets/4-b-decay-table-1.png){width=80%}

---

![](assets/4-b-decay-table-2.png){width=80%}

---

\begin{align*}
    & {}^{218}_{84}Po \rightarrow {}^{218}_{85}At + {}^{0}_{-1}e \\
    & {}^{218}_{84}Po \rightarrow {}^{218}_{85}At + {}^{0}_{-1}\beta \\
\end{align*}

---

In a gamma emission, the decaying atom only loses energy, which means it has the same number of protons and neutrons afterwards.

---

![](assets/4-g-decay-table-1.png){width=80%}

---

![](assets/4-g-decay-table-2.png){width=80%}

---

\begin{align*}
    & {}^{201}_{79}Au \rightarrow {}^{201}_{79}Au + \gamma \\
\end{align*}

---

Write down the equations for:

1. An alpha decay of Polonium-218
2. A beta decay of Hydrogen-3
3. A gamma decay of Carbon-14
4. An absorption of a neutron by Carbon-13

---

5. A double alpha decay of Uranium-234
6. A double alpha decay of Radon-222
7. A double beta decay of Thorium-234
8. A double beta decay of Bismuth-210

---
