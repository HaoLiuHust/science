---
title: Atomic and Nuclear Physics
date: 2019-09-17
url: /12phy/4-nuclear/
---

## Slides

1. [Atoms & Isotopes](slides/1-atoms-and-isotopes/) [(PDF)](pdfs/1-atoms-and-isotopes.pdf)
2. [Discovering the Atomic Structure](slides/2-atomic-structure/) [(PDF)](pdfs/2-atomic-structure.pdf)
  - Homework Booklet: 2004 Q2, 2005 Q1-2, 2006 Q1, 2007 Q1
  - Worksheet 1: Questions 1-4
  - Worksheet 3: Questions 1-2
3. [Radioactive Decay](slides/3-radioactive-decay) [(PDF)](pdfs/3-radioactive-decay.pdf)
  - Worksheet 1: Questions 5-10
  - Worksheet 2: Questions 1-3, 4a, 4b, 5a, 5b, 6a, 6b
4. [Nuclear Equations](slides/4-nuclear-equations) [(PDF)](pdfs/4-nuclear-equations.pdf)
  - Homework Booklet: 2004 Q1, 2004 Q4, 2005 Q5
  - Worksheet 2: Questions: 4c, 5c, 6c 7-9
5. [Half-Life](slides/5-half-life) [(PDF)](pdfs/5-half-life.pdf)
  - Homework Booklet: 2004 Q3, 2005 Q7, 2006 Q4, 2005 Q9
  - Worksheet 3: Questions: 3-6
6. [Fission & Fusion](slides/6-fission-and-fusion) [(PDF)](pdfs/6-fission-and-fusion.pdf)

[Download All PDFs](4-nuclear.zip)
