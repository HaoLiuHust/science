---
title: 12 Physics
subtitle: Welcome to 12 Physics
date: 2019-08-17
---

1. Waves
2. [Mechanics](/12phy/2-mechanics)
3. [Electricity](/12phy/3-electricity)
4. [Nuclear and Atomic Physics](/12phy/4-nuclear)
