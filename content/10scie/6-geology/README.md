# Year 10 Science: Geology

## Learning Outcomes

1. Explain how the Earth has changed over 4.6 billion years and account for its current structure.
2. Explain the theory of plate tectonics using many sources of evidence.
3. Use scientific language: __crust, mantle, outer core, inner core, subduction zone, constructive, destructive, and conservative plate boundaries__.
4. Use plate tectonics to explain why and where earthquakes and volcanic eruptions happen.
5. Recall major seiesmic events from NZ and Earth history and relate them to their impact on people.
6. Use scientific language: __Richter and Mercalli scale, epicenter, focus, p and s waves, viscosity of lava, shield, dome and caldera__.
7. Describe how sedimentary (including fossils), metamorphic, and igneous rocks are created and destroyed.
8. Explain how land features (mountains, rivers, plains) are linked to tectonic plate movement errosion and weathering.