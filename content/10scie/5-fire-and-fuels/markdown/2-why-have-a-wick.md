---
title: Why Have a Wick?
subtitle: 10SCIE - Fire & Fuels
author: Finn LeSueur
date: 2019
theme: finn
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
- \usepackage{textcomp}
---

# Learning Outcomes

- To be able to explain the function of a wick

---

# What do you think a wick is for?

---

## Walking Water Demonstration

---

### Capillary Action

- Liquids can flow into narrow spaces, even against the direction of gravity.
- E.g. paper in water, paint brushes sucking up water, a thin tube in water.
- The surface tension and adhesive forces between the liquid and container propel the water into the narrow space.

---

![](assets/2-capillary-action.png){ width=75% }

---

![](assets/2-how-things-ignite.jpg){ width=75% }

---

## Wick Practical

1. Safety
2. Practical read through
3. Check understanding each step
4. Explain must do Qs 1,2,3 during practical
5. Timeframe (20mins)
6. Clean up check

---

### Discussion Points

1. Why didn’t the solid paraffin wax ignite?
2. Did the liquid wax ignite? Why/ why not?
3. How does the wick allow the wax to burn?
      (think about last lesson’s practical)
4. Why doesn’t the wick burn?
    (clue:  evaporation and sweating)

