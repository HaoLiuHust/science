---
title: Balancing Equations
subtitle: 10SCIE - Fire & Fuels
author: Finn LeSueur
date: 2019
theme: finn
colortheme: dolphin
font-size: 35px
text-align: center
header-includes:
- \usepackage{graphicx}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{amsmath}
- \usepackage{textcomp}
---

# Learning Outcomes

8. Write the word equation for complete combustion
9. Write the balanced symbol equation for complete combustion

---

# Starter

1. What are some key observations that would indicate incomplete combustion?
2. What are some key observations that would indicate complete combustion?

---

## Keywords

- __Reactants:__ Butane, oxygen, hydrogen, vinegar etc.
- __Products:__ Carbon dioxide, water, carbon etc.
- __Chemical Compound:__ $H_{2}O$, $CO_{2}$, $NH_{3}$, $NaCl$ etc.
- __Atom/Molecule__: H, C, O, Au, Ca

---

### Question

What happens to the atoms/molecules during a chemical reaction?

- No new atoms are created or destroyed
- Mass of the reactants = mass of the products
- The reactants change molecular structure
- Two or more new products are made
- The atoms are _rearranged_

---

### Complete Combustion of Methane

Complete the word and balanced symbol equation for this reaction.

\begin{align*}
    & methane \quad + \quad oxygen \rightarrow \hspace{2cm} + \hspace{2cm} \\
    & CH_{4} \quad + \quad O_{2} \rightarrow \hspace{2cm} + \hspace{2cm}
\end{align*}

---

#### Answer

Complete combustion of methane:

\begin{align*}
    & methane \quad + \quad oxygen \rightarrow \quad \text{carbon dioxide} + \quad water \\
    & CH_{4} \quad + \quad 2O_{2} \rightarrow \quad CO_{2} + \quad 2H_{2}O
\end{align*}

---

### Burning of Magnesium

Complete the word and balanced symbol equation for this reaction.

\begin{align*}
    & magnesium \quad + \quad oxygen \rightarrow \hspace{2cm} \\
    & Mg \quad + \quad O_{2} \rightarrow \hspace{2cm}
\end{align*}

---

#### Answer

Burning of Magnesium

\begin{align*}
    & magnesium \quad + \quad oxygen \rightarrow \quad \text{magnesium oxide} \\
    & 2Mg \quad + \quad O_{2} \rightarrow \quad 2MgO
\end{align*}

---

### Balance these reactions:

\begin{align*}
    & C \quad + \quad O_{2} \rightarrow \quad CO_{2} \\
    & H_{2} \quad + \quad O_{2} \rightarrow \quad H_{2}O \\
    & N_{2} \quad + \quad H_{2} \rightarrow \quad NH_{3} \\
    & CH_{4} \quad + \quad O_{2} \rightarrow \quad CO_{2} + \quad H_{2}O
\end{align*}

---

#### Answer

\begin{align*}
    & C \quad + \quad O_{2} \rightarrow \quad CO_{2} \\
    & 2H_{2} \quad + \quad O_{2} \rightarrow \quad 2H_{2}O \\
    & N_{2} \quad + \quad 3H_{2} \rightarrow \quad 2NH_{3} \\
    & CH_{4} \quad + \quad 2O_{2} \rightarrow \quad CO_{2} + \quad 2H_{2}O
\end{align*}

---

### Extra Work

1. Page 9: Fuels & Combustion
2. Balancing Equations Worksheet
