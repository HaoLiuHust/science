---
title: Fire & Fuels
date: 2019-08-17
url: /10scie/5-fire-and-fuels/
---

## Slides

1. [Introduction](slides/1-introduction/) [(PDF)](pdfs/1-introduction.pdf)
2. [Why Have a Wick](slides/2-why-have-a-wick/) [(PDF)](pdfs/2-why-have-a-wick.pdf)
3. [Burning](slides/3-burning/) [(PDF)](pdfs/3-burning.pdf)
4. [Products of Combustion](slides/4-products-of-combustion/) [(PDF)](pdfs/4-products-of-combustion.pdf)
5. [Fire Extinguishers](slides/5-fire-extinguishers/) [(PDF)](pdfs/5-fire-extinguishers.pdf)
6. [Complete and Incomplete Combustion](slides/6-complete-incomplete-combustion/) [(PDF)](pdfs/6-complete-incomplete-combustion.pdf)
7. [Hydrocarbons](slides/7-hydrocarbons/) [(PDF)](pdfs/7-hydrocarbons.pdf)
8. [Balancing Equations](slides/8-balancing-equations/) [(PDF)](pdfs/8-balancing-equations.pdf)
9. [Environmental Impact](slides/9-environmental-impact/) [(PDF)](pdfs/9-environmental-impact.pdf)

[Download All PDFs](5-fire-and-fuels.zip)

## Learning Outcomes

1. Describe the fire triangle
2. List the three things needed for a fire to occur
3. Explain that fuels must be in the gas phase to burn
4. Explain how a fire is extinguished and relate it to the fire triangle
5. Describe how a wick works and its function
6. Descirbe the differences between complete and incomplete combustion
7. Explain the disadvantages and dangers of incomplete combustion
8. Write the word equation for complete combustion
9. Write the balanced symbol equation for complete combustion
10. Determine the energy produced per gram when burning fuels
11. Recognise that a hydrocarbon contains only C and H atoms
12. Name and write the formula for the first six hydrocarbons
13. Explain the greenhous effect and its advantages and disadvantages
14. List the greenhouse gasses
15. Explain how burning fuels is contributing to the greenhouse effect
16. List air pollutants which are produced from fossil fuels and describe their effects on human health

## Lesson Sequence

### 1. Unit Introduction

__Kaupapa:__ _Perform some basic combustion experiments and get introduced to Fire & Fuels._

__Starter:__

Video: [Firefighters put jet fuel on a fire](https://www.youtube.com/watch?v=UxC2OOSEPyQ)

What temperature does these fuels start burning? Put them in order from coolest to hottest

- Petrol
- LPG
- Wood
- Coal
- Candle

__Notes:__ [Introduction](slides/1-introduction/) [(PDF)](pdfs/1-introduction.pdf)

__Practical:__ Workbook: Burning Things (pg. 2)

---

### 2. Why Have A Wick?

__Kaupapa:__ _What is the purpose of a wick?_

__Starter:__ Walking water demonstration

__Notes:__ [Why Have a Wick](slides/2-why-have-a-wick/) [(PDF)](pdfs/2-why-have-a-wick.pdf)

__Practical:__ Workbook: Why Have a Wick (pg. 4)
