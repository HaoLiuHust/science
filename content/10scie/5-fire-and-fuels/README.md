# Year 10 Science: Fire & Fuels

## Learning Outcomes

1. Describe the fire triangle
2. List the three things needed for a fire to occur
3. Explain that fuels must be in the gas phase to burn
4. Explain how a fire is extinguished and relate it to the fire triangle
5. Describe how a wick works and its function
6. Descirbe the differences between complete and incomplete combustion
7. Explain the disadvantages and dangers of incomplete combustion
8. Write the word equation for complete combustion
9. Write the balanced symbol equation for complete combustion
10. Determine the energy produced per gram when burning fuels
11. Recognise that a hydrocarbon contains only C and H atoms
12. Name and write the formula for the first six hydrocarbons
13. Explain the greenhous effect and its advantages and disadvantages
14. List the greenhouse gasses
15. Explain how burning fuels is contributing to the greenhouse effect
16. List air pollutants which are produced from fossil fuels and describe their effects on human health