# ![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg) [https://science.lesueur.nz](https://science.lesueur.nz)

## What is This?

These notes and resources are all a work-in-progress. I am completing them as I teach each unit and each year group, so it is likely that you will find them incomplete in many areas.

Please feel free to head over to my git repositories to get a copy of all my resources, to contribute, or just to report bugs.

### Credits

Repository logo made by [Freepik](https://www.flaticon.com/authors/freepik) from [www.flaticon.com](https://www.flaticon.com) as part of the [Renewable Energy Pack](https://www.flaticon.com/packs/reneweable-energy-1) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)